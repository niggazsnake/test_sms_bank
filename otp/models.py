from datetime import datetime, timedelta

from django.conf import settings
from django.db import models


class UserOtpCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='User')
    code = models.CharField('Code', max_length=settings.OTP_CODE_LENGTH)
    date_created = models.DateTimeField('Date created/updated', auto_now=True)

    class Meta:
        verbose_name = 'User OTP code'
        verbose_name_plural = 'User OTP codes'

    def __str__(self):
        return self.code

    @classmethod
    def verify_code(cls, user, code):
        date_check = datetime.now() - timedelta(minutes=settings.OTP_CODE_TIME_LIFE)
        is_valid_code = cls.objects.filter(user=user, code=code, date_created__gte=date_check).exists()
        return is_valid_code

    @classmethod
    def clean_user_code(cls, user):
        cls.objects.filter(user=user).delete()

    @classmethod
    def create_or_update_sms_code(cls, user, code):
        otp_user = cls.objects.filter(user=user).first()
        if otp_user:
            otp_user.code = code
            otp_user.save()
        else:
            cls.objects.create(user=user, code=code)