import random


class OTPGenerator:
    digits = "0123456789"
    code_length = 4

    def __init__(self):
        self.otp = ""

    def generate(self):
        self.otp = ""
        for i in range(self.code_length):
            self.otp += self.digits[int(random.random() * 10)]
        return self.otp
