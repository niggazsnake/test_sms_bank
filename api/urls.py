from django.urls import path

from .views import RegisterPhoneView
from .views import VerifySmsCodeView

urlpatterns = [
    path('register-phone/', RegisterPhoneView.as_view(), name='register-phone'),
    path('verify-phone/', VerifySmsCodeView.as_view(), name='verify-phone'),
]