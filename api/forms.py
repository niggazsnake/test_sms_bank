from django import forms
from django.conf import settings
from user_auth.models import CustomUser


class PhoneForm(forms.Form):
    user = forms.ModelChoiceField(queryset=CustomUser.objects.all())
    code = forms.CharField(label='Country code', max_length=5)
    phone = forms.CharField(label='Phone Number', max_length=10)


class SmsCodeForm(forms.Form):
    code = forms.CharField(label='SMS code', max_length=settings.OTP_CODE_LENGTH)
    user = forms.ModelChoiceField(queryset=CustomUser.objects.all())
