from django.conf import settings
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from otp.generators import OTPGenerator
from otp.models import UserOtpCode
from .forms import PhoneForm
from .forms import SmsCodeForm


class BasePhoneView(View):
    http_method_names = ['post', ]
    form_class = None
    HTTP_201 = '201'
    HTTP_400 = '400'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def _process_form(self, form):
        raise NotImplemented

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            answer, status = self._process_form(form)
        else:
            answer = {'success': False, 'errors': form.errors}
            status = self.HTTP_400
        return JsonResponse(answer, status=status)


class RegisterPhoneView(BasePhoneView):
    form_class = PhoneForm

    def _send_sms_with_code(self):
        pass

    def _process_form(self, form):
        answer = {'success': True}
        code = OTPGenerator().generate()
        user = form.cleaned_data.get('user')
        UserOtpCode.create_or_update_sms_code(user, code)
        self._send_sms_with_code()
        if settings.DEBUG:
            print(code)
        return answer, self.HTTP_201


class VerifySmsCodeView(BasePhoneView):
    form_class = SmsCodeForm

    def _process_form(self, form):
        code = form.cleaned_data.get('code')
        user = form.cleaned_data.get('user')
        is_valid_code = UserOtpCode.verify_code(user=user, code=code)
        if is_valid_code:
            user.sms_verified = True
            user.save(update_fields=['sms_verified', ])
            answer = {'success': True}
            status = self.HTTP_201
            UserOtpCode.clean_user_code(user)
        else:
            answer = {'success': False, 'errors': 'Sms code is invalid or expired'}
            status = self.HTTP_400
        return answer, status
